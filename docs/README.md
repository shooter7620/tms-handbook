---
home: true
heroImage: /PBST-Logo.png
actionText: Let's start →
actionLink: tms/
---

Welcome to the Mayhem Syndicate, we unite as brothers in arms and cause chaos to large groups and militaries all across Robloxia. Using high tech weapons and alien technology, we bend those who oppose us to our will.

We primarily celebrate destruction as the Pinewood Computer Core raid group, where we attack the core and don’t let PBST get in our way.

::: danger WARNING!
Please read through all of the rules very carefully. Any changes to the rules will be made clear. The rules have been categorized into who they apply to, however some rules may apply to more than one group of TMS Members, in which case it will be made clear. Punishments for breaking a rule depend on what rule and the severity. Punishments can range from a simple warning, demotion, or even blacklist from TMS.
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>
